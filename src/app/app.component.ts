import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'false';
  flag = true;
  indice = -1;
  personas = [
    { nombre: "david", posicion: "Practicante", edad: 23 },
    { nombre: "jhon", posicion: "Developer", edad: 22 },
    { nombre: "deysi", posicion: "Practicante", edad: 19 }
  ]

  model: any = {};

  guardarPersona() {
    if (this.model.nombre == "undefined") {
      alert("Vacio");

    } else {
      alert(this.model.nombre)
      this.personas.push(this.model);
      this.model = {};
    }


  }
  editarPersona(i) {
    this.model.nombre = this.personas[i].nombre;
    this.model.posicion = this.personas[i].posicion;
    this.model.edad = this.personas[i].edad;
    this.indice = i;
    this.flag = false;
  }
  eliminarPersona(i) {
    this.personas.splice(i, 1);

  }
  actualizarPersona() {

    for (let index = 0; index < this.personas.length; index++) {
      if (index == this.indice) {
        this.personas[index].nombre = this.model.nombre;
        this.personas[index].posicion = this.model.posicion;
        this.personas[index].edad = this.model.edad;
      }

    }
    this.indice = -1;
    this.flag = true;
    this.model = {};

  }
}
